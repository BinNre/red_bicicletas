const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');

passport.use(new LocalStrategy(
    function(email, password, done) {
        Usuario.findOne({ email: email }, function(err, usuario) {
            if (err) return done(err);
            if (!usuario) return done(null, false, { message: 'El email es incorrecto o no existe' })
            if (!usuario.validPassword(password)) return done(null, false, { message: 'La contraseña es incorrecta' });

            return done(null, usuario);
        });
    }
));

passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});


passport.serializeUser(function(id, cb) {
    Usuario.findById(id, function(err, usuario) {
        cb(err, user.id);
    });
});

module.exports = passport;