var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const saltRounds = 10;
const bcrypt = require('bcrypt');
const uniqueValidator = require('mongoose-unique-validator');
const crypto = require('crypto');

const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)+@\w+([\.-]?\w+)+(\.\w{2,3})+$/;
    return re.test(email);
};


var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombrer es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un emaiil valido'],
        match: [/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }

});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function(next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);;

    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
    var reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta, cb: cb });
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString() });
    const email_destination = this.email;
    token.save(function(err) {
        if (err) { return console.log(err.message); }

        const mailOption = {
            form: 'no-replay@redbicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verficar su ceunta haga click en este enlace: \n' + 'https://localhost:5000' + '\/token/confirmation\/' + token.token + '.\n'
        };
        mailer.sendMail(mailOption, function(err) {
            if (err) { return console.log(err.message); }

            console.log('Se ha enviado un email de bienvenida a:' + email_destination);
        });
    });
}


module.exports = mongoose.model('Usuario', usuarioSchema);